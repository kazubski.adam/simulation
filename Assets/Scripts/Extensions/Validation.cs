﻿namespace Extensions
{
    public static class Validation
    {
        public static bool IsNot (this ref bool currentValue, bool newValue) {
            if (currentValue == newValue) return false;

            currentValue = newValue;
            return true;
        }
    }
}