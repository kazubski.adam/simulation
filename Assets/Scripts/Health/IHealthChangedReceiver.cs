namespace Health
{
    public interface IHealthChangedReceiver
    {
        public void OnHealthChanged (Health health);
    }
}