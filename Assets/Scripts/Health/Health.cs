using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cache = Essentials.Cache;

namespace Health
{
    public class Health
    {
        private readonly List<IHealthChangedReceiver> _receivers = new();
        private readonly GameObject _newTargetGameObject;

        private float _value;
        private float _currentSwitchTime;
        private Coroutine _switchCoroutine;

        public bool IsTakingDamage { get; private set; } = true;
        public float MaxValue { get; set; }

        public float Value
        {
            get => _value;
            set {
                if (!IsTakingDamage) return;

                _value = value;
                CallReceivers();
            }
        }

        private Health () { }

        public Health (GameObject newTargetGameObject) {
            _newTargetGameObject = newTargetGameObject;
        }

        public void SwitchTakingDamage (bool switchTo) => IsTakingDamage = switchTo;

        public void DontTakeDamageFor (float newWaitTime, WaitType waitType) {
            switch (waitType) {
                case WaitType.CurrentOnly:
                    if (_currentSwitchTime <= 0) _currentSwitchTime = newWaitTime;
                    break;
                case WaitType.NewOnly:
                    _currentSwitchTime = newWaitTime;
                    break;
                case WaitType.SumUp:
                    _currentSwitchTime += newWaitTime;
                    break;
            }

            if (_switchCoroutine != null) return;

            _switchCoroutine = Cache.StartCoroutine(WaitForTakingDamage());
        }

        private IEnumerator WaitForTakingDamage () {
            while (_currentSwitchTime > 0) {
                if (!_newTargetGameObject.activeSelf) break;

                _currentSwitchTime -= Cache.DeltaTime;
                yield return null;
            }

            IsTakingDamage = true;
            _currentSwitchTime = 0;
            _switchCoroutine = null;
        }

        private void CallReceivers () {
            for (var i = 0; i < _receivers.Count; i++) { _receivers[i].OnHealthChanged(this); }
        }

        public void AddReceiver (IHealthChangedReceiver receiver, bool call = false) {
            if (!_receivers.Contains(receiver)) _receivers.Add(receiver);

            if (call) receiver.OnHealthChanged(this);
        }

        public void RemoveReceiver (IHealthChangedReceiver receiver) {
            if (_receivers.Contains(receiver)) _receivers.Remove(receiver);
        }
    }

    public enum WaitType
    {
        CurrentOnly, NewOnly, SumUp
    }
}