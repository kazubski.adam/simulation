using PolyAndCode.UI;
using UnityEngine;

namespace Numbers
{
    public struct NumberInfo
    {
        public int index;
        public string word;
    }

    public class NumbersScroller : MonoBehaviour, IRecyclableScrollRectDataSource
    {
        private const string _THREE_WORD = "Marko";
        private const string _FIVE_WORD = "Polo";
        private const string _BOTH_WORD = "MarkoPolo";

        private bool _isInitialized;
    
        [SerializeField]
        RecyclableScrollRect _recyclableScrollRect;

        private readonly NumberInfo[] _numbersInfo = new NumberInfo[100];

        private void Awake () => _recyclableScrollRect.DataSource = this;

        public void Generate () {
            for (var i = 0; i < _numbersInfo.Length; i++) {
                var newInfo = new NumberInfo {
                    index = i + 1,
                    word = GetWord(i + 1)
                };

                _numbersInfo[i] = newInfo;
            }
        
            if(_isInitialized)
                _recyclableScrollRect.ReloadData(this);
            else {
                _isInitialized = true;
                _recyclableScrollRect.Initialize(this);
            }

            string GetWord (int index) {
                var isThree = index >= 3 && index % 3 == 0;
                var isFive = index >= 5 && index % 5 == 0;

                return isThree switch {
                    true when isFive => _BOTH_WORD,
                    true => _THREE_WORD,
                    false when isFive => _FIVE_WORD,
                    _ => string.Empty
                };
            }
        }

        public int GetItemCount () => _numbersInfo.Length;

        public void SetCell (ICell cell, int index) {
            var item = cell as NumberCell;
            item.ConfigureCell(_numbersInfo[index]);
        }
    }
}