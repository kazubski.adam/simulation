using PolyAndCode.UI;
using TMPro;
using UnityEngine;

namespace Numbers
{
    public class NumberCell : MonoBehaviour, ICell
    {
        [SerializeField] private TMP_Text _index;
        [SerializeField] private TMP_Text _word;
    
        public void ConfigureCell(NumberInfo numberInfo)
        {
            _index.text = numberInfo.index.ToString();
            _word.text = numberInfo.word;
        }
    }
}