using Base.Abstraction.Selection;
using Health;
using TMPro;
using UnityEngine;

namespace Base_Agent
{
    public class BaseAgentInfo : SelectionManager, IHealthChangedReceiver
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private TMP_Text _agentName;
        [SerializeField] private TMP_Text _agentHealthPoints;

        public override void OnSelect () => _canvas.enabled = true;
        public override void OnDeselect () => _canvas.enabled = false;
    
        public void ChangeAgentName (string newName) => _agentName.text = newName;

        public void OnHealthChanged (Health.Health health) =>
            _agentHealthPoints.text = health.Value + "/" + health.MaxValue;
    }
}