﻿using Base.Abstraction.Selection;
using Base.Abstraction.Spawning;
using Base_Agent.Spawn;
using Base_Agent.States;
using Extensions;
using UnityEditor;
using UnityEngine;

namespace Base_Agent
{
    public class BaseAgent : SpawnItem<BaseSpawnController, BaseSpawnSettings, BaseAgent>
    {
        [field: SerializeField] public Rigidbody Rigidbody { get; private set; }
        [field: SerializeField] public BaseAgentSettings AgentSettings { get; private set; }
        [field: SerializeField] public SelectionTarget SelectionTarget { get; private set; }

        [SerializeField] [HideInInspector] private bool _hasAgentSettings;
    
        public BaseAgentState currentState;
        public Health.Health health;
    
        private BaseAgentStateFactory _stateFactory;
    
        public Vector3 RandomPosition => 
            _SpawnController.SpawnSettings.GetRandomPositionFrom(transform.position);
    
        private void Awake () {
            health = new Health.Health(gameObject);
            _stateFactory = new(this);
        }
    
        protected override void OnSetController () =>
            SelectionTarget.SetManager(_SpawnController.BaseAgentInfo);

        public void Prepare () {
            currentState = _stateFactory.spawning;
            currentState.OnEnterState();
        }
    
        private void FixedUpdate () {
            if (!_hasAgentSettings) return;

            currentState.OnFixedUpdate();
        }
    
        private void OnCollisionEnter (Collision collision) {
            if (!_hasAgentSettings) return;
        
            currentState.OnCollisionEnter(collision);
        }

        public void ReleaseItem () => _SpawnController.ReleaseItem(this);
        
        public void OnSelectionEnter (SelectionTarget selectionTarget) {
            health.AddReceiver(_SpawnController.BaseAgentInfo, true);
            _SpawnController.BaseAgentInfo.ChangeAgentName(gameObject.name);
        }

        public void OnSelectionExit (SelectionTarget selectionTarget) {
            health.RemoveReceiver(_SpawnController.BaseAgentInfo);
        }

        #if UNITY_EDITOR
        [SerializeField] [HideInInspector] private BaseAgentSettings _previousSettings;
    
        private void OnValidate () {
            if (_hasAgentSettings.IsNot(AgentSettings != null)) EditorUtility.SetDirty(gameObject);
        
            if (AgentSettings != _previousSettings) {
                _previousSettings = AgentSettings;
            
                EditorUtility.SetDirty(gameObject);
            }
        }
        #endif
    }
}