﻿using Base.Abstraction.Spawning;
using UnityEditor;
using UnityEngine;

namespace Base_Agent.Spawn
{
    [CreateAssetMenu(menuName = "Scriptable Objects/Base Spawn Settings",
                     fileName = "New Base Spawn Settings", order = 0)]
    public class BaseSpawnSettings : SpawnSettings<BaseSpawnController, BaseSpawnSettings, BaseAgent>
    {
        [Header("Additional Spawn Settings")]
        [SerializeField] private float _spawnAreaSize;
        [SerializeField] private float _searchTargetPositionMinDistance;
        [SerializeField] private float _searchTargetPositionMinAngle;
        [SerializeField] private float _searchTargetPositionMaxAngle;
        [SerializeField] private float _searchTargetPositionAngleStep;
        [SerializeField] private int _searchTargetSaveCounter;

        [SerializeField] [HideInInspector] private Vector3[] _centers;

        public Vector3 GetRandomPositionFrom (Vector3 originPosition) {
            var targetDistance = GetTargetDistance();
            var positionOffset = Vector3.forward * targetDistance;

            var startAngle = Random.Range(_searchTargetPositionMinAngle, _searchTargetPositionMaxAngle);
            var angleOffset = _searchTargetPositionAngleStep;
            var multiplier = 1;
            var saveCounter = 0;
            var newPosition = GetPoint();

            while (!IsNewPositionValid()) {
                #if UNITY_EDITOR
                if (Debug) {
                    var color = Color.Lerp(Color.blue, Color.red,
                                           (float) saveCounter / _searchTargetSaveCounter);

                    UnityEngine.Debug.DrawLine(newPosition, newPosition + Vector3.up, color, 5);
                }
                #endif

                if (saveCounter > _searchTargetSaveCounter) {
                    #if UNITY_EDITOR
                    if (Debug) UnityEngine.Debug.Log("Could not find any valid position!");
                    #endif
                    newPosition = GetRandomPosition();
                    break;
                }

                multiplier = -multiplier;
                newPosition = GetPoint();

                if (multiplier == -1) angleOffset += _searchTargetPositionAngleStep;
                saveCounter++;
            }

            #if UNITY_EDITOR
            if (Debug) UnityEngine.Debug.DrawLine(newPosition, newPosition + Vector3.up * 4, Color.green);
            #endif
        
            return newPosition;

            float GetTargetDistance () {
                var maxDistance = 0f;
                var minDistance = 0f;
                var validDistancesCount = 0;

                for (var i = 0; i < _centers.Length; i++) {
                    var newDistance = Vector3.Distance(originPosition, _centers[i]);
                    if (newDistance > maxDistance) maxDistance = newDistance;
                    else if (newDistance > _searchTargetPositionMinDistance) validDistancesCount++;
                    else minDistance = _searchTargetPositionMinDistance;
                }

                if (validDistancesCount > 0)
                    return Random.Range(_searchTargetPositionMinDistance, maxDistance);

                return Random.Range(minDistance, maxDistance);
            }

            Vector3 GetPoint () {
                var angle = Vector3.up * (startAngle + angleOffset * multiplier);
                return originPosition + Quaternion.Euler(angle) * positionOffset;
            }

            bool IsNewPositionValid () {
                var x = newPosition.x;
                var z = newPosition.z;

                return x <= _spawnAreaSize && x >= -_spawnAreaSize &&
                       z <= _spawnAreaSize && z >= -_spawnAreaSize;
            }
        }

        public Vector3 GetRandomPosition () {
            var x = Random.Range(-_spawnAreaSize, _spawnAreaSize);
            var z = Random.Range(-_spawnAreaSize, _spawnAreaSize);

            return new Vector3(x, 0, z);
        }

        #if UNITY_EDITOR
        [field: SerializeField] public bool Debug { get; private set; }

        protected new void OnValidate () {
            base.OnValidate();

            _centers = new Vector3[] {
                new(0, 0, _spawnAreaSize),
                new(_spawnAreaSize, 0, 0),
                new(0, 0, -_spawnAreaSize),
                new(-_spawnAreaSize, 0, 0)
            };

            EditorUtility.SetDirty(this);
        }
        #endif
    }
}