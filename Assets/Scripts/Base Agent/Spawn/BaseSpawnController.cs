﻿using Base.Abstraction.Spawning;
using UnityEngine;

namespace Base_Agent.Spawn
{
    public class BaseSpawnController : SpawnController<BaseSpawnController, BaseSpawnSettings, BaseAgent>
    {
        [SerializeField] private BaseAgentInfo _baseAgentInfo;

        public BaseAgentInfo BaseAgentInfo => _baseAgentInfo;

        protected override void OnGetItem (BaseAgent item) {
            item.Prepare();
            item.transform.position = _spawnSettings.GetRandomPosition();
            item.SelectionTarget.enabled = true;
            item.gameObject.SetActive(true);
        }

        protected override void OnReleaseItem (BaseAgent item) {
            item.SelectionTarget.enabled = false;
            item.gameObject.SetActive(false);
        }
    }
}