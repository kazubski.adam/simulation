﻿using UnityEngine;

namespace Base_Agent
{
    [CreateAssetMenu(menuName = "Scriptable Objects/Base Agent Settings", fileName = "New Base Agent Settings", order = 0)]
    public class BaseAgentSettings : ScriptableObject
    {
        [Header("Health")]
        [SerializeField] private int _startHealthPoints;

        [Header("Spawning")]
        [SerializeField] private float _spawnDuration;
        [SerializeField] private AnimationCurve _spawnAnimationCurve;

        [Header("Dying")]
        [SerializeField] private float _dyingDuration;
        [SerializeField] private AnimationCurve _dyingAnimationCurve;

        [Header("Movement/Rotation")]
        [SerializeField] private float _movementRotationStep;
        [SerializeField] private float _movementMaxRotationSpeed;
        [SerializeField] private float _movementStoppingRotationAngle;
    
        [Header("Movement/Movement")]
        [SerializeField] private float _movementAccelerationStep;
        [SerializeField] private float _movementMaxMagnitude;
        [SerializeField] private float _movementDecelerationDistance;
    
        [Header("Waiting")]
        [SerializeField] private float _waitTimeMin;
        [SerializeField] private float _waitTimeMax;

        [Header("Struck")]
        [SerializeField] private float _struckMinDuration;
        [SerializeField] private float _struckMaxDuration;
        [SerializeField] private float _struckFallbackVelocity;
        [SerializeField] private float _struckFallbackDuration;
        [SerializeField] private float _onStruckDontTakeDamageFor;

        public int StartHealthPoints => _startHealthPoints;

        public float SpawnDuration => _spawnDuration;
        public AnimationCurve SpawnAnimationCurve => _spawnAnimationCurve;

        public float DyingDuration => _dyingDuration;
        public AnimationCurve DyingAnimationCurve => _dyingAnimationCurve;

        public float StruckFallbackVelocity => _struckFallbackVelocity;
        public float StruckFallbackDuration => _struckFallbackDuration;
        public float OnStruckDontTakeDamageFor => _onStruckDontTakeDamageFor;

        public float MovementRotationStep => _movementRotationStep;
        public float MovementMaxRotationSpeed => _movementMaxRotationSpeed;
        public float MovementStoppingRotationAngle => _movementStoppingRotationAngle;
    
        public float MovementAccelerationStep => _movementAccelerationStep;
        public float MovementMaxMagnitude => _movementMaxMagnitude;
        public float MovementDecelerationDistance => _movementDecelerationDistance;

        public float RandomWaitTime => Random.Range(_waitTimeMin, _waitTimeMax);
        public float RandomStruckTime => Random.Range(_struckMinDuration, _struckMaxDuration);
        public float Fall => Random.Range(_struckMinDuration, _struckMaxDuration);

        #if UNITY_EDITOR
        [field: SerializeField] public bool Debug { get; private set; }
        #endif
    }
}