using Health;
using UnityEngine;

namespace Base_Agent.States
{
    public abstract class BaseAgentState
    {
        protected readonly BaseAgent context;
        protected readonly BaseAgentStateFactory factory;

        public BaseAgentState PreviousState { get; private set; }
        public bool WasPaused { get; private set; }
    
        private BaseAgentState () { }

        protected BaseAgentState (BaseAgent newContext, BaseAgentStateFactory newFactory) {
            context = newContext;
            factory = newFactory;
        }
    
        protected void SwitchTo (BaseAgentState newState, bool pauseCurrentState = false) {
            WasPaused = pauseCurrentState;

            if (pauseCurrentState) OnPauseState();
            else OnExitState();
        
            if (newState.WasPaused) newState.OnUnpauseState();
            else newState.OnEnterState();

            newState.PreviousState = this;
            newState.WasPaused = false;
        
            context.currentState = newState;
        }

        public abstract void OnEnterState ();
        public abstract void OnExitState ();
        public abstract void OnPauseState ();
        public abstract void OnUnpauseState ();

        public abstract void OnFixedUpdate ();
    
        public abstract void OnCollisionEnter (Collision collision);

        protected void OnCollisionEntered (Collision collision) {
            context.health.Value--;

            if (context.health.Value <= 0) {
                SwitchTo(factory.dying);
                return;
            }

            var contactPoint = collision.contacts[0].point;
            var struckFallbackDirection = (context.transform.position - contactPoint).normalized;
            struckFallbackDirection.y = 0;
            var onStruckDontTakeDamageFor = context.AgentSettings.OnStruckDontTakeDamageFor;

            factory.beingStruck.fallbackDirection = struckFallbackDirection;
            factory.beingStruck.currentStruckTime = context.AgentSettings.RandomStruckTime;
            factory.beingStruck.currentFallBackTime = context.AgentSettings.StruckFallbackDuration;
            context.Rigidbody.velocity = Vector3.zero;
        
            if (context.health.IsTakingDamage)
                context.health.DontTakeDamageFor(onStruckDontTakeDamageFor, WaitType.CurrentOnly);

            SwitchTo(factory.beingStruck);
        }
    }
}