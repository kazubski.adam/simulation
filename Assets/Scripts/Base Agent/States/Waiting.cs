using UnityEngine;

namespace Base_Agent.States
{
    public class Waiting : BaseAgentState
    {
        public float currentWaitTime;

        public Waiting (BaseAgent newContext, BaseAgentStateFactory newFactory) :
            base(newContext, newFactory) { }

        public override void OnEnterState () => context.Rigidbody.velocity = Vector3.zero;
        public override void OnExitState () { }
        public override void OnPauseState () => context.Rigidbody.velocity = Vector3.zero;
        public override void OnUnpauseState () { }

        public override void OnFixedUpdate () {
            if (currentWaitTime > 0) {
                currentWaitTime -= Time.fixedDeltaTime;
                return;
            }

            var newPosition = context.RandomPosition;
            factory.moving.targetPosition = newPosition;
            SwitchTo(factory.moving);
        }

        public override void OnCollisionEnter (Collision collision) => OnCollisionEntered(collision);
    }
}