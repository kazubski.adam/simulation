namespace Base_Agent.States
{
    public class BaseAgentStateFactory
    {
        private BaseAgentStateFactory() { }

        public BaseAgentStateFactory (BaseAgent context) {
            spawning = new Spawning(context, this);
            moving = new Moving(context, this);
            waiting = new Waiting(context, this);
            beingStruck = new BeingStruck(context, this);
            dying  = new Dying(context, this);
        }
    
        public readonly Spawning spawning;
        public readonly Moving moving;
        public readonly Waiting waiting;
        public readonly BeingStruck beingStruck;
        public readonly Dying dying;
    }
}