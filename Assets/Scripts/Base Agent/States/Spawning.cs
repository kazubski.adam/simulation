using UnityEngine;

namespace Base_Agent.States
{
    public class Spawning : BaseAgentState
    {
        private float _currentSpawnTime;

        public Spawning (BaseAgent newContext, BaseAgentStateFactory newFactory) :
            base(newContext, newFactory) { }

        public override void OnEnterState () {
            _currentSpawnTime = context.AgentSettings.SpawnDuration;
            context.health.SwitchTakingDamage(false);
            context.transform.localScale = Vector3.zero;
            context.transform.localRotation = Quaternion.Euler(Vector3.up * Random.Range(0f, 360f));
            context.Rigidbody.velocity = Vector3.zero;
        }

        public override void OnExitState () { }
        public override void OnPauseState () { }
        public override void OnUnpauseState () { }

        public override void OnFixedUpdate () {
            var agentSettings = context.AgentSettings;

            _currentSpawnTime -= Time.fixedDeltaTime;

            var completion = 1 - _currentSpawnTime / agentSettings.SpawnDuration;
            completion = agentSettings.SpawnAnimationCurve.Evaluate(completion);

            #if UNITY_EDITOR
            if (agentSettings.SpawnDuration == 0) completion = 1;
            #endif

            context.transform.localScale = Vector3.one * completion;

            if (_currentSpawnTime > 0) return;

            context.health.SwitchTakingDamage(true);
            context.health.MaxValue = agentSettings.StartHealthPoints;
            context.health.Value = agentSettings.StartHealthPoints;
            factory.waiting.currentWaitTime = context.AgentSettings.RandomWaitTime;
            SwitchTo(factory.waiting);
        }

        public override void OnCollisionEnter (Collision collision) { }
    }
}