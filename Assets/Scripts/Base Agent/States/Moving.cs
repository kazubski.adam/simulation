using UnityEngine;
using Cache = Essentials.Cache;

namespace Base_Agent.States
{
    public class Moving : BaseAgentState
    {
        public Vector3 targetPosition;
        
        private float _currentAccelerationStep;
        private float _currentRotationStep;
        private bool _isLookingAtTarget;

        public Moving (BaseAgent newContext, BaseAgentStateFactory newFactory) :
            base(newContext, newFactory) { }

        public override void OnEnterState () {
            _currentAccelerationStep = 0;
            _currentRotationStep = 0;
            _isLookingAtTarget = false;
        }

        public override void OnExitState () { }
        public override void OnPauseState () { }
        public override void OnUnpauseState () { }

        public override void OnFixedUpdate () {
            #if UNITY_EDITOR
            if(context.AgentSettings.Debug) 
                Debug.DrawLine(targetPosition, targetPosition + Vector3.up * 4, Color.green);
            #endif
        
            if(_isLookingAtTarget) ManageMovement();
            else ManageRotation();
        }

        private void ManageMovement () {
            var agentSettings = context.AgentSettings;
            var rigidbody = context.Rigidbody;
            var currentPosition = context.transform.position;
            var currentDistance = Vector3.Distance(targetPosition, currentPosition);
            var movementDirection = (targetPosition - currentPosition).normalized;

            // Finished Moving
            if (currentDistance <= 0.02f) {
                rigidbody.velocity = Vector3.zero;
                factory.waiting.currentWaitTime = context.AgentSettings.RandomWaitTime;
                SwitchTo(factory.waiting);
                return;
            }

            // Apply velocity
            _currentAccelerationStep += agentSettings.MovementAccelerationStep;
            var nextVelocity = Vector3.ClampMagnitude(GetNewVelocity(), GetMaxMagnitude());
            rigidbody.AddForce(nextVelocity - rigidbody.velocity, ForceMode.VelocityChange);

            Vector3 GetNewVelocity () {
                var nextPosition = currentPosition + movementDirection * _currentAccelerationStep;
                var toGo = Vector3.Distance(currentPosition, nextPosition);
                return movementDirection * (toGo / Cache.FixedDeltaTime);
            }

            float GetMaxMagnitude () {
                if (currentDistance > agentSettings.MovementDecelerationDistance)
                    return agentSettings.MovementMaxMagnitude;

                var completion = currentDistance / agentSettings.MovementDecelerationDistance;
                return agentSettings.MovementMaxMagnitude * completion;
            }
        }

        private void ManageRotation () {
            var agentSettings = context.AgentSettings;
            _currentRotationStep += agentSettings.MovementRotationStep;
            
            var clampedRotationStep = Mathf.Clamp(_currentRotationStep, 0, 
                                                  agentSettings.MovementMaxRotationSpeed);

            var localTargetPosition = context.transform.InverseTransformPoint(targetPosition);
            var degreesToGo = Mathf.Atan2(localTargetPosition.x, localTargetPosition.z) * Mathf.Rad2Deg;
            var normalizedDegreesToGo = Mathf.Abs(degreesToGo);
            
            // Manage Easing Out
            if (normalizedDegreesToGo <= agentSettings.MovementStoppingRotationAngle) {
                var completion = normalizedDegreesToGo / agentSettings.MovementStoppingRotationAngle;

                // Finished Rotating
                if (completion < 0.02f) {
                    clampedRotationStep = degreesToGo;
                    _isLookingAtTarget = true;
                }
                else clampedRotationStep *= completion;
            }

            // Rotate by step
            var directionMultiplier = degreesToGo >= 0 ? 1 : -1;
            var angularVelocity = new Vector3(0, clampedRotationStep * directionMultiplier, 0);
            var rotationStep = Quaternion.Euler(angularVelocity);
            context.Rigidbody.MoveRotation(context.Rigidbody.rotation * rotationStep);
        }

        public override void OnCollisionEnter (Collision collision) => OnCollisionEntered(collision);
    }
}