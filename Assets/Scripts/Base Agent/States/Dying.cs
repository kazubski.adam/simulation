﻿using UnityEngine;

namespace Base_Agent.States
{
    public class Dying : BaseAgentState
    {
        private float _currentDyingTime;
    
        public Dying (BaseAgent newContext, BaseAgentStateFactory newFactory) :
            base(newContext, newFactory) { }

        public override void OnEnterState () {
            _currentDyingTime = context.AgentSettings.DyingDuration;
            context.health.SwitchTakingDamage(false);
        }
    
        public override void OnExitState () { }
        public override void OnPauseState () { }
        public override void OnUnpauseState () { }

        public override void OnFixedUpdate () {
            var agentSettings = context.AgentSettings;

            _currentDyingTime -= Time.fixedDeltaTime;

            var completion = _currentDyingTime / agentSettings.DyingDuration;
            completion = agentSettings.DyingAnimationCurve.Evaluate(completion);

            #if UNITY_EDITOR
            if (agentSettings.DyingDuration == 0) completion = 1;
            #endif

            context.transform.localScale = Vector3.one + Vector3.one * completion;

            if (_currentDyingTime > 0) return;
        
            context.ReleaseItem();
        }

        public override void OnCollisionEnter (Collision collision) { }
    }
}