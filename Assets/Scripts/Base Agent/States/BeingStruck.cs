using UnityEngine;

namespace Base_Agent.States
{
    public class BeingStruck : BaseAgentState
    {
        public float currentStruckTime;
        public float currentFallBackTime;
        public Vector3 fallbackDirection;

        public BeingStruck (BaseAgent newContext, BaseAgentStateFactory newFactory) :
            base(newContext, newFactory) { }

        public override void OnEnterState () { }
        public override void OnExitState () { }
        public override void OnPauseState () { }
        public override void OnUnpauseState () { }

        public override void OnFixedUpdate () {
            ManageFallback();
        
            if (currentStruckTime > 0) {
                currentStruckTime -= Time.fixedDeltaTime;
                return;
            }

            factory.moving.targetPosition = context.RandomPosition;
            SwitchTo(factory.moving);

            void ManageFallback () {
                if (currentFallBackTime <= 0) {
                    currentFallBackTime = 0;
                    context.Rigidbody.velocity = Vector3.zero;
                    return;
                }

                currentFallBackTime -= Time.fixedDeltaTime;
            
                var rigidbody = context.Rigidbody;
                var agentSettings = context.AgentSettings;
                var completion = currentFallBackTime / agentSettings.StruckFallbackDuration;
                var nextVelocity = fallbackDirection * (agentSettings.StruckFallbackVelocity * completion);
                rigidbody.AddForce(nextVelocity - rigidbody.velocity, ForceMode.VelocityChange);
            }
        }
    
        public override void OnCollisionEnter (Collision collision) => OnCollisionEntered(collision);
    }
}