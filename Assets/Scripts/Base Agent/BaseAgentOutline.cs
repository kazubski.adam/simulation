using Base.Abstraction.Selection;
using Base.Outline;
using UnityEngine;

namespace Base_Agent
{
    public class BaseAgentOutline : Outline
    {
        [SerializeField] private OutlineSettings _settings;

        public void OnSelectionTargetStateChanged (SelectionTarget selectionTarget) =>
            UpdateOutline(selectionTarget.IsHovered, selectionTarget.IsSelected);

        private void UpdateOutline (bool isHovered, bool isSelected) {
            if (isHovered && isSelected) ChangeSettings(_settings.SelectedHoveredData);
            else if (isHovered) ChangeSettings(_settings.HoveredData);
            else if (isSelected) ChangeSettings(_settings.SelectedData);
            else SwitchOutline(false);
        }

        private void ChangeSettings (OutlineData outlineData) {
            OutlineWidth = outlineData.Width;
            OutlineColor = outlineData.Color;
            OutlineMode = outlineData.OutlineMode;
        
            SwitchOutline(true);
        }

        private void SwitchOutline (bool switchTo) => enabled = switchTo;
    }
}