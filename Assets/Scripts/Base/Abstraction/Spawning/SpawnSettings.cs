﻿using Extensions;
using UnityEditor;
using UnityEngine;

namespace Base.Abstraction.Spawning
{
    public abstract class SpawnSettings<TController, TSettings, TItem> : ScriptableObject
        where TController : SpawnController<TController, TSettings, TItem>
        where TSettings : SpawnSettings<TController, TSettings, TItem>
        where TItem : SpawnItem<TController, TSettings, TItem>
    {
        [Header("Base References")]
        [SerializeField] private TItem _spawnItemPrefab;

        [Header("Base Spawn Settings")]
        [SerializeField] private float _spawnTimeStepMin;
        [SerializeField] private float _spawnTimeStepMax;
        [SerializeField] private int _spawnItemsMaxCount;

        [SerializeField] [HideInInspector] private bool _hasSpawnItemPrefab;

        public float SpawnTimeStepMin => _spawnTimeStepMin;
        public float SpawnTimeStepMax => _spawnTimeStepMax;
        public int SpawnItemsMaxCount => _spawnItemsMaxCount;

        public virtual bool CanSpawn () => _hasSpawnItemPrefab;
        public virtual TItem GetNewItem() => Instantiate(_spawnItemPrefab);

        #if UNITY_EDITOR
        protected void OnValidate () {
            if (_hasSpawnItemPrefab.IsNot(_spawnItemPrefab != null)) EditorUtility.SetDirty(this);
        }
        #endif
    }
}