﻿using UnityEngine;

namespace Base.Abstraction.Spawning
{
    public abstract class SpawnItem<TController, TSettings, TItem> : MonoBehaviour
        where TController : SpawnController<TController, TSettings, TItem>
        where TSettings : SpawnSettings<TController, TSettings, TItem>
        where TItem : SpawnItem<TController, TSettings, TItem>
    {
        protected TController _SpawnController { get; private set; }

        public void SetController (TController newSpawnController) {
            _SpawnController = newSpawnController;
            OnSetController();
        }

        protected virtual void OnSetController () { }
    }
}