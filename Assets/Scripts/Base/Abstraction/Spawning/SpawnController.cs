using Extensions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Pool;

namespace Base.Abstraction.Spawning
{
    public abstract class SpawnController<TController, TSettings, TItem> : MonoBehaviour
        where TController : SpawnController<TController, TSettings, TItem>
        where TSettings : SpawnSettings<TController, TSettings, TItem>
        where TItem : SpawnItem<TController, TSettings, TItem>
    {
        [SerializeField] protected TSettings _spawnSettings;
        [SerializeField] [HideInInspector] protected bool _hasSpawnSettings;

        protected ObjectPool<TItem> spawnItems;
        protected float currentTime;
        protected float targetTime;

        protected int activeCount;

        public TSettings SpawnSettings => _spawnSettings;
        public bool HasSpawnSettings => _hasSpawnSettings;

        private void Awake () => CreatePool();
        private void Start () => SetForShouldSpawn();
        private void Update () => SpawnItems();
    
        private void CreatePool () => 
            spawnItems = new ObjectPool<TItem>(GetNewItem, OnGetItem, OnReleaseItem);
    
        protected void SpawnItems () {
            UpdateForShouldSpawn();
        
            if (!_hasSpawnSettings || !_spawnSettings.CanSpawn() || !ShouldSpawn()) return;

            activeCount++;
            spawnItems.Get().SetController((TController) this);
            SetForShouldSpawn();
        }

        private TItem GetNewItem () => _spawnSettings.GetNewItem();
        protected abstract void OnGetItem (TItem item);
        protected abstract void OnReleaseItem (TItem item);

        public void ReleaseItem (TItem item) {
            spawnItems.Release(item);
            activeCount--;
        }
    
        protected virtual void UpdateForShouldSpawn () => currentTime += Time.deltaTime;

        protected virtual bool ShouldSpawn () {
            var timeIsRight = currentTime >= targetTime;
            var countIsRight = activeCount < _spawnSettings.SpawnItemsMaxCount;

            return timeIsRight && countIsRight;
        }

        protected virtual void SetForShouldSpawn () {
            currentTime = 0;
            targetTime = Random.Range(_spawnSettings.SpawnTimeStepMin, _spawnSettings.SpawnTimeStepMax);
        }

        #if UNITY_EDITOR
        [SerializeField] [HideInInspector] private TSettings _previousSettings;
    
        private void OnValidate () {
            if (_hasSpawnSettings.IsNot(_spawnSettings != null)) EditorUtility.SetDirty(gameObject);

            if (_spawnSettings != _previousSettings) {
                _previousSettings = _spawnSettings;
            
                EditorUtility.SetDirty(gameObject);
            }
        }
        #endif
    }
}