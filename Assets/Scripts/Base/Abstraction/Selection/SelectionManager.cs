using Attributes;
using Essentials;
using UnityEngine;

namespace Base.Abstraction.Selection
{
    public class SelectionManager : MonoBehaviour
    {
        [TagSelector]
        [SerializeField] private string[] _ignoreDeselectionTags;

        public SelectionTarget HoveredTarget { get; private set; }
        public SelectionTarget SelectedTarget { get; private set; }
        public bool HasTargetHovered { get; private set; }
        public bool HasTargetSelected { get; private set; }
    
        private bool _isOn = true;

        public void SwitchManager (bool switchTo) {
            _isOn = switchTo;
            // todo manage currently hovered and selected
        }
    
        private void Update () {
            if (!_isOn) return;
            if (!Input.GetMouseButtonUp(0)) return;

            ManageDeselection();
            ManageSelection();

            void ManageDeselection () {
                if (!HasTargetSelected) return;
                if (!ShouldDeselect(CustomInputModule.GetCurrentObject())) return;

                Deselect();
            }

            void ManageSelection () {
                if (!HasTargetHovered) return;
                Select(HoveredTarget);
            }
        }

        private bool ShouldDeselect (GameObject targetObject) {
            if (targetObject is null) return true;

            for (var i = 0; i < _ignoreDeselectionTags.Length; i++)
                if (targetObject.CompareTag(_ignoreDeselectionTags[i])) return false;
        
            return true;
        }

        public void ChangeHoveredTargetTo (SelectionTarget target) {
            target.OnHoverEnter();
            HoveredTarget = target;
            HasTargetHovered = true;
        }

        public void ChangeHoveredTargetToNone () {
            if (!HasTargetHovered) return;
        
            HoveredTarget.OnHoverExit();
            HoveredTarget = null;
            HasTargetHovered = false;
        }

        public void Select (SelectionTarget target) {
            target.OnSelectionEnter();
            SelectedTarget = target;
            HasTargetSelected = true;
            OnSelect();
        }

        public void Deselect () {
            if (!HasTargetSelected) return;
        
            SelectedTarget.OnSelectionExit();
            SelectedTarget = null;
            HasTargetSelected = false;
            OnDeselect();
        }

        public virtual void OnSelect () { }
        public virtual void OnDeselect () { }
    }
}