using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Cache = Essentials.Cache;

namespace Base.Abstraction.Selection
{
    public class SelectionTarget : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private SelectionManager _manager;
        private bool _hasManager;

        public bool IsHovered { get; private set; }
        public bool IsSelected { get; private set; }

        [SerializeField] private UnityEvent<SelectionTarget> _onHoveredEnter;
        [SerializeField] private UnityEvent<SelectionTarget> _onHoveredExit;
        [SerializeField] private UnityEvent<SelectionTarget> _onSelectionEnter;
        [SerializeField] private UnityEvent<SelectionTarget> _onSelectionExit;

        public void SetManager (SelectionManager newManager) {
            if (_manager == newManager) return;
        
            if (_hasManager) {
                if (IsHovered) _manager.ChangeHoveredTargetToNone();
                else if (IsSelected) _manager.Deselect();
            }
        
            if (newManager is null) {
                _hasManager = false;
                return;
            }

            _manager = newManager;
            _hasManager = true;
        
            if (IsHovered) _manager.ChangeHoveredTargetTo(this);
        }

        public void OnPointerEnter (PointerEventData eventData) {
            IsHovered = true;
            if (!_hasManager) return;
            _manager.ChangeHoveredTargetTo(this);
        }

        public void OnPointerExit (PointerEventData eventData) {
            IsHovered = false;
            if (!_hasManager) return;
            _manager.ChangeHoveredTargetToNone();
        }

        public void OnHoverEnter () => _onHoveredEnter.Invoke(this);
        public void OnHoverExit () => _onHoveredExit.Invoke(this);

        public void OnSelectionEnter () {
            IsSelected = true;
            _onSelectionEnter.Invoke(this);
        }

        public void OnSelectionExit () {
            IsSelected = false;
            _onSelectionExit.Invoke(this);
        }

        private void OnDisable () {
            #if UNITY_EDITOR
            if (Cache.CurrentPlayModeState != PlayModeStateChange.EnteredPlayMode) return;
            #endif
        
            if (!_hasManager) return;
            if (IsHovered) _manager.ChangeHoveredTargetToNone();
            else if (IsSelected) _manager.Deselect();
        }
    }
}