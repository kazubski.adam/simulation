using UnityEngine;

namespace Base.Outline
{
    [CreateAssetMenu(menuName = "Scriptable Objects/Base Agent Outline Settings", 
                     fileName = "New Base Agent Outline Settings", order = 0)]
    public class OutlineSettings : ScriptableObject
    {
        [SerializeField] private OutlineData _hoveredData;
        [SerializeField] private OutlineData _selectedData;
        [SerializeField] private OutlineData _selectedHoveredData;

        public OutlineData HoveredData => _hoveredData;
        public OutlineData SelectedData => _selectedData;
        public OutlineData SelectedHoveredData => _selectedHoveredData;
    }
}