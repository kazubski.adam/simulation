using System;
using UnityEngine;

namespace Base.Outline
{
    [Serializable]
    public class OutlineData
    {
        [SerializeField] private float _width;
        [SerializeField] private Color _color;
        [SerializeField] private global::Outline.Mode _outlineMode;

        public float Width => _width;
        public Color Color => _color;
        public global::Outline.Mode OutlineMode => _outlineMode;
    }
}