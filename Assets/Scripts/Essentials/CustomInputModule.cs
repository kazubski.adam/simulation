using UnityEngine;
using UnityEngine.EventSystems;

namespace Essentials
{
    public class CustomInputModule : StandaloneInputModule
    {
        private static CustomInputModule _instance;

        protected override void Awake () {
            if (_instance != null) {
                Destroy(this);
                return;
            }

            _instance = this;
            DontDestroyOnLoad(this);
            base.Awake();
        }

        private GameObject GameObjectUnderPointer(int pointerId)
        {
            var lastPointer = GetLastPointerEventData(pointerId);
            return lastPointer?.pointerCurrentRaycast.gameObject;
        }
 
        public static GameObject GetCurrentObject() => _instance.GameObjectUnderPointer(kMouseLeftId);
    }
}