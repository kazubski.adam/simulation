using System.Collections;
using UnityEditor;
using UnityEngine;

namespace Essentials
{
    public class Cache : MonoBehaviour
    {
        private static Cache _instance;
    
        #if UNITY_EDITOR
        public static PlayModeStateChange CurrentPlayModeState { get; private set; }
        #endif
    
        public static float FixedDeltaTime { get; private set; }
        public static float DeltaTime { get; private set; }

        private void Awake () {
            _instance = this;
        }

        public void Update () {
            FixedDeltaTime = Time.fixedDeltaTime;
            DeltaTime = Time.deltaTime;
        }

        private Coroutine BaseStartCoroutine (IEnumerator coroutine) => base.StartCoroutine(coroutine);
        private void BaseStopCoroutine (Coroutine coroutine) => base.StopCoroutine(coroutine);
    
        public new static Coroutine StartCoroutine (IEnumerator enumerator) => 
            _instance.BaseStartCoroutine(enumerator);

        public new static void StopCoroutine (Coroutine coroutine) =>
            _instance.BaseStopCoroutine(coroutine);
    
        #if UNITY_EDITOR
        public void OnValidate () => EditorApplication.playModeStateChanged += (state) =>
        {
            CurrentPlayModeState = state;
        };
        #endif
    }
}